import { Request, Response } from "express";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import User from "../models/user.model";
import config from "../config";

class AuthController {
  register = async (req: Request, res: Response) => {
    const { username, password } = req.body;
    try {
      const user = await User.findOne({ username });
      if (user) {
        return res.status(400).json({ message: "User already exists" });
      }

      const newUser = new User({ username, password });
      const salt = await bcrypt.genSalt(10);
      newUser.password = await bcrypt.hash(password, salt);
      await newUser.save();

      res.status(201).json({ message: "User registered successfully" });
    } catch (err) {
      res.status(500).json({ message: "Server error" });
    }
  };

  login = async (req: Request, res: Response) => {
    const { username, password } = req.body;
    try {
      const user = await User.findOne({ username });
      if (!user) {
        return res.status(400).json({ msg: "Invalid credentials" });
      }

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res.status(400).json({ msg: "Invalid credentials" });
      }

      const payload = { user: { id: user.id, username: user.username } };
      const token = jwt.sign(payload, config.jwtSecret, { expiresIn: 3600 });

      res.json({ token, user: { id: user.id, username: user.username } });
    } catch (err) {
      res.status(500).json({ message: "Server error" });
    }
  };

  getUserProfile = async (req: Request, res: Response) => {
    try {
      if(!req.user) return res.status(401).json({ msg: "No token, authorization denied" });

      const user = await User.findById(req.user.id).select("-password");
      if (!user) {
        return res.status(404).json({ msg: "User not found" });
      }
      res.json(user);
    } catch (err) {
      res.status(500).json({ msg: "Server error" });
    }
  };
}

const authController = new AuthController();

export default authController;
