import { Request, Response } from "express";
import { Util } from "../utility/common.util";
import { Sudoku } from "../sudoku";
import sudokuService from "../services/app.service";

class SudokuController {
  async puzzle(req: Request, res: Response): Promise<Response> {
    try {
      let sudoku = new Sudoku();
      let puzzle = sudoku.puzzle;
      return res.status(200).send({ game: puzzle });
    } catch (error) {
      return res.status(500).send({ message: "Error while generating puzzle" });
    }
  }
  async solve(req: Request, res: Response): Promise<Response> {
    try {
      let puzzle = [];
      Util.copyGrid(req.body.board, puzzle);
      let sudoku = new Sudoku(puzzle);
      let solution = sudoku.isSolvable();
      let solvedSudoku;
      let status;
      if (solution) {
        solvedSudoku = sudoku.solvedPuzzle;
        status = true;
        sudokuService.addScore(req.user?.id);
      } else {
        solvedSudoku = req.body.board;
        status = false;
      }
      return res.status(200).send({ solution: solvedSudoku, status: status });
    } catch (error) {
      return res.status(500).send({ message: "Error while solving puzzle" });
    }
  }
  async validate(req: Request, res: Response): Promise<Response> {
    try {
      let puzzle = [];
      Util.copyGrid(req.body.board, puzzle);
      let sudoku = new Sudoku(puzzle);
      let status = sudoku.validate();

      if(status) {
        sudokuService.addScore(req.user?.id);
      }

      return res.status(200).send({ status: status });
    } catch (error) {
      return res.status(500).send({ message: "Error while validating puzzle" });
    }
  }

  async leaderboard(req: Request, res: Response): Promise<Response> {
    try {
      let leaderboard = await sudokuService.getLeaderboard();
      return res.status(200).send({ leaderboard: leaderboard });
    } catch (error) {
      return res
        .status(500)
        .send({ message: "Error while fetching leaderboard" });
    }
  }

  async clearDb(req: Request, res: Response): Promise<Response> {
    try {
      await sudokuService.clearDb();
      return res.status(200).send({ message: "Database cleared" });
    } catch (error) {
      return res.status(500).send({ message: "Error while clearing database" });
    }
  }
}

const sudokuController = new SudokuController();

export default sudokuController;
