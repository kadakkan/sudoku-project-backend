import mongoose from "mongoose";
import userModel from "../models/user.model";

class SudokuService {
    async addScore(userId) {
        try {
            console.log(userId)
            const a =userModel.findByIdAndUpdate(new mongoose.Types.ObjectId(userId), { $inc: { score: 100 } }).exec();
            console.log(a)
            return;
        } catch (error) {
            console.log(error);
            throw new Error("Error while Adding Score");
        }
    }

    async getLeaderboard() {
        try {
            let leaderboard = await userModel.find().sort({ score: -1 }).select('username score').limit(10).lean();
            return leaderboard;
        } catch (error) {
            console.log(error);
            throw new Error("Error while fetching leaderboard");
        }
    }

    async clearDb() {
        try {
            await userModel.deleteMany({});
            return;
        } catch (error) {
            console.log(error);
            throw new Error("Error while clearing database");
        }
    }
}

const sudokuService = new SudokuService();

export default sudokuService;