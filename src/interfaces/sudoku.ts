import mongoose, { Document, Schema } from "mongoose";
import { z } from "zod";

// export interface Book  {
//   _id: Schema.Types.ObjectId;
//   title: string;
//   author: string;
//   createdAt: Date;
// }

export enum Difficulty {
  EASY = 6,
  NORMAL = 9,
  HARD = 12,
}

// export const bookZodSchema = z.object({
//   body: z.object({
//     title: z.string(),
//     author: z.string(),
//     genre: z.nativeEnum(BookGenre),
//   }),
// });
