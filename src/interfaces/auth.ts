import mongoose, { Document, Schema } from "mongoose";
import { z } from "zod";

export interface User  {
  username: string;
  password: string;
  createdAt: Date;
}

export const userRegisterZodSchema = z.object({
  body: z.object({
    username: z.string(),
    password: z.string(),
  }),
});
export const userLoginZodSchema = z.object({
  body: z.object({
    username: z.string(),
    password: z.string(),
  }),
});
