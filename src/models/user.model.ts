import mongoose, { Schema } from "mongoose";

export interface IUser extends Document {
  username: string;
  password: string;
  score?: number;
}

const UserSchema: Schema = new Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  score: { type: Number, default: 0 },
});

export default mongoose.model<IUser>("User", UserSchema);
