import User from "./../models/user.model";
import { Request, Response, Router } from "express";
import { validate } from "../middleware/validate";
import { userLoginZodSchema, userRegisterZodSchema } from "../interfaces/auth";
import authController from "../controllers/auth.controller";
import { auth } from "../middleware/authentication";

const router = Router();

router.post(
  "/register",
  [validate(userRegisterZodSchema)],
  (req: Request, res: Response) => authController.register(req, res)
);

router.post(
  "/login",
  [validate(userLoginZodSchema)],
  async (req: Request, res: Response) => authController.login(req, res)
);

router.get("/user/profile", auth, async (req: Request, res: Response) =>
  authController.getUserProfile(req, res)
);

export default router;
