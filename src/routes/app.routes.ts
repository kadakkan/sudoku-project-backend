import { Request, Response, Router } from "express";
import { validate } from "../middleware/validate";
import sudokuController from "../controllers/sudoku.controller";
import { auth } from "../middleware/authentication";

const router = Router();

router.get("/puzzle", (req: Request, res: Response) =>
  sudokuController.puzzle(req, res)
);

router.post("/solve", [auth], (req: Request, res: Response) =>
  sudokuController.solve(req, res)
);

router.post("/validate", [auth], (req: Request, res: Response) =>
  sudokuController.validate(req, res)
);

router.get("/protected", [auth], (req: Request, res: Response) => {
  res.json({ message: "Protected route" });
});

router.get("/leaderBoard", (req: Request, res: Response) => {
  sudokuController.leaderboard(req, res);
});

router.get("/clearDb", (req: Request, res: Response) => {
  sudokuController.clearDb(req, res);
});

export default router;
